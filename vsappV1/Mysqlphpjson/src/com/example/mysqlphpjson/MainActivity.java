package com.example.mysqlphpjson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.os.AsyncTask;


public class MainActivity extends Activity {
	private String jsonResult;
	private String url = "http://10.167.34.31:8090/MySQLConnection.php";
	private ListView listView;

	// User Session Manager Class
	UserSessionManager session;

	// Button Logout
	Button btnLogout;

	// button Notify
	Button btnNotify;

	// JsonSQL variables
	// public Map<String, String> usernameDepartmentMap = new HashMap<String,
	// String>();
	// public Map<String, String> usernameLocationMap = new HashMap<String,
	// String>();
	//public Map<String, Bitmap> usernameImageMap = new HashMap<String, Bitmap>();

	// Employee list notification
	List<Map<String, String>> employeeList = new ArrayList<Map<String, String>>();

	String username = "";
	String name = "";
	String department = "";
	String location = "";
	String image = "";

	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		accessWebService();

		// Session class instance
		session = new UserSessionManager(getApplicationContext());

		TextView lblName = (TextView) findViewById(R.id.lblName);
		TextView lblLocation = (TextView) findViewById(R.id.lblLocation);
		TextView lblDepartment = (TextView) findViewById(R.id.lblDepartment);

		// Button logout
		btnLogout = (Button) findViewById(R.id.btnLogout);

		// Button that triggers notification
		// btnNotify = (Button) findViewById(R.id.button1);

		Toast.makeText(getApplicationContext(),
				"User Login Status: " + session.isUserLoggedIn(),
				Toast.LENGTH_LONG).show();

		// Check user login
		// If User is not logged in , This will redirect user to LoginActivity.
		if (session.checkLogin())
			finish();

		// get user data from session
		HashMap<String, String> user = session.getUserDetails();

		username = user.get(UserSessionManager.KEY_USERNAME);

		// get name
		name = user.get(UserSessionManager.KEY_NAME);

		// get email
		department = user.get(UserSessionManager.KEY_DEPARTMENT);

		location = user.get(UserSessionManager.KEY_LOCATION);
		
		image = user.get(UserSessionManager.KEY_IMAGE);
		
		//decoding base64 here, then converting to bitmap for ImageView
		byte[] imagebyte = Base64.decode(image,Base64.DEFAULT); 
		Bitmap imageBitmap = BitmapFactory.decodeByteArray(imagebyte, 0,
		  imagebyte.length);
		
		// Image
		ImageView imageViewProfile = (ImageView) findViewById(R.id.imageView1);
		imageViewProfile.setImageBitmap(imageBitmap);

		listView = (ListView) findViewById(R.id.listView1);

		lblName.setText(Html.fromHtml("Name: <b>" + name + "</b>"));
		lblDepartment.setText(Html.fromHtml("Department: <b>" + department
				+ "</b>"));
		lblLocation.setText(Html.fromHtml("Location <b>" + location + "</b>"));

		SimpleAdapter simpleAdapter = new SimpleAdapter(this, employeeList,
				android.R.layout.simple_selectable_list_item,
				new String[] { "employees" }, new int[] { android.R.id.text1 });
		listView.setAdapter(simpleAdapter);

		btnLogout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				// Clear the User session data when logout button is clicked
				// and redirect user to LoginActivity
				session.logoutUser();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getMenuInflater();
		getMenuInflater().inflate(R.menu.main, menu);
		
		//Linny's added code for navigation bar
	    inflater.inflate(R.menu.activity_main_actions, menu);
	 
	    return super.onCreateOptionsMenu(menu);
		
	}
	
	/**
	 * On selecting action bar icons
	 * This method accepts menu item as a parameter. Selected item
   	 * can be identified by using it's ID.
     * Handle action bar item clicks here. The action bar will
     * automatically handle clicks on the Home/Up button, so long
     * as you specify a parent activity in AndroidManifest.xml.
	 */
   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);*/
    	
    	//Take appropriate action for each action item click
    /*	switch (item.getItemId()){
    		
    	case R.id.action_home:
    		//home
    		openProfile();
    		return true;
    	case R.id.settings_2_icon:
    		//settings
    		openSettings();
    		return true;
    	case R.id.action_email:
    		openMail();
    		return true;
    	case R.id.action_person:
    		openSearchPerson();
    		return true;
    	case R.id.action_logout:
    		//log out
    		//openLogout();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	
    	}
    }
    */
    /**
     * Launching new activity
     */
    /*private void LocationFound(){
     * 
     * loads new activity such as profile page
    	Intent i = new Intent(MainActivity.this, LocationFound.class);
    	startActivity(i);
    }*/
    
    private void openProfile(){
    	Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
    }
    
    private void openSettings(){
    	
    	//Toast.makeText(this,"This is user's settings", Toast.LENGTH_SHORT).show();
    	Intent i = new Intent(this, ButtonActivity.class);
    	startActivity(i);
    }
    
    private void openMail(){
    	Toast.makeText(this,"This is user's timeline", Toast.LENGTH_SHORT).show();
    }
    
    private void openSearchPerson(){
    	//Toast.makeText(this,"User can search for employee(s)", Toast.LENGTH_SHORT).show();
    	Intent i = new Intent(this, SearchActivity.class);
    	startActivity(i);
    }
	// Async Task to access the php web service
	private class JsonReadTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(params[0]);
			try {
				HttpResponse response = httpclient.execute(httppost);
				jsonResult = inputStreamToString(
						response.getEntity().getContent()).toString();
			}

			catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		private StringBuilder inputStreamToString(InputStream is) {
			String rLine = "";
			StringBuilder answer = new StringBuilder();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));

			try {
				while ((rLine = rd.readLine()) != null) {
					answer.append(rLine);
				}
			}

			catch (IOException e) {
				// e.printStackTrace();
				Toast.makeText(getApplicationContext(),
						"Error..." + e.toString(), Toast.LENGTH_LONG).show();
			}
			return answer;
		}

		@Override
		protected void onPostExecute(String result) {
			JsonRetrievalData();
		}
	}// end async task

	public void accessWebService() {
		JsonReadTask task = new JsonReadTask();
		// passes values for the urls string array
		task.execute(new String[] { url });
	}

	// build hash set for list view
	@SuppressLint("InlinedApi")
	public void JsonRetrievalData() {
		try {
			JSONObject jsonResponse = new JSONObject(jsonResult);
			JSONArray jsonMainNode = jsonResponse.optJSONArray("users");

			for (int i = 0; i < jsonMainNode.length(); i++) {
				JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
				
				  /*String imageColumn = jsonChildNode.optString("image");
				  byte[] imagebyte = Base64.decode(imageColumn.getBytes(), i); 
				  Bitmap imageBitmap = BitmapFactory.decodeByteArray(imagebyte, 0,
				  imagebyte.length);
				  usernameImageMap.put(jsonChildNode.optString("username"),
				  imageColumn);
				 */
				String outPut = jsonChildNode.optString("Full_Name").toString()
						+ " has joined VCE on "
						+ jsonChildNode.optString("start_date").toString();
				employeeList.add(createEmployee("employees", outPut));
			}
		} catch (JSONException e) {
			Toast.makeText(getApplicationContext(), "Error" + e.toString(),
					Toast.LENGTH_SHORT).show();
		}

	}

	private HashMap<String, String> createEmployee(String name,
			String start_date) {
		HashMap<String, String> employeeNameStartDate = new HashMap<String, String>();
		employeeNameStartDate.put(name, start_date);
		return employeeNameStartDate;
	}

}
