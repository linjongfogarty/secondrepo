package com.example.mysqlphpjson;
 
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
import java.util.ArrayList;
import java.util.HashMap;
 
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
 
public class SearchActivity extends Activity {
     
    // List view
    private ListView lv;
     
    // Listview Adapter
    ArrayAdapter<String> adapter;
     
    // Search EditText
    EditText inputSearch;
     
     
    // ArrayList for Listview
    ArrayList<HashMap<String, String>> userList;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);
         
        // Listview Data
        String users[] = {"", "", ""};
         
        lv = (ListView) findViewById(R.id.list_view);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
         
        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.user_profile, users);
        lv.setAdapter(adapter);       
         
    }
     
}