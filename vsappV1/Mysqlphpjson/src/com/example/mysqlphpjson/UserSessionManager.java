package com.example.mysqlphpjson;


import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;

public class UserSessionManager {

	// Shared Preferences reference
	SharedPreferences pref;

	// Editor reference for Shared preferences
	Editor editor;

	// Context
	Context context;

	// Shared preference mode
	int PRIVATE_MODE = 0;

	// Shared preference file name
	private static final String PREFER_NAME = "AndroidExamplePref";

	// All Shared Preferences Keys
	private static final String IS_USER_LOGIN = "IsUserLoggedIn";
	
	public static final String KEY_USERNAME = "username";

	// User name (make variable public to access from outside)
	public static final String KEY_NAME = "name";

	// Email address (make variable public to access from outside)
	public static final String KEY_LOCATION = "location";
	
	public static final String KEY_DEPARTMENT = "department";
	
	public static final String KEY_IMAGE = "image";

	// Constructor
	@SuppressLint("CommitPrefEdits")
	public UserSessionManager(Context context) {
		this.context = context;
		pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	// Create login session
	public void createUserLoginSession(String username, String name, String department, String location, String image) {
		// Storing login value as TRUE
		editor.putBoolean(IS_USER_LOGIN, true);

		editor.putString(KEY_USERNAME, username);
		
		// Storing name in pref
		editor.putString(KEY_NAME, name);

		// Storing email in pref
		editor.putString(KEY_DEPARTMENT, department);
		
		editor.putString(KEY_LOCATION, location);
		
		editor.putString(KEY_IMAGE, image);
		

		// commit changes
		editor.commit();
	}

	/*
	 * Check login method will check user login status If false it will redirect
	 * user to login page Else do anything
	 */

	public boolean checkLogin() {
		// Check login status
		if (!this.isUserLoggedIn()) {

			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(context, LoginActivity.class);

			// Closing all the Activities from stack
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Staring Login Activity
			context.startActivity(i);

			return true;
		}
		return false;
	}

	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails() {

		// Use hashmap to store user credentials
		HashMap<String, String> user = new HashMap<String, String>();
		
		//username
		user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));

		// user Full_name
		user.put(KEY_NAME, pref.getString(KEY_NAME, null));

		// user email id
		user.put(KEY_DEPARTMENT, pref.getString(KEY_DEPARTMENT, null));
		
		user.put(KEY_LOCATION, pref.getString(KEY_LOCATION, null));
		
		user.put(KEY_IMAGE, pref.getString(KEY_IMAGE, null));

		// return user
		return user;
	}

	/**
	 * Clear session details
	 * */
	public void logoutUser() {

		// Clearing all user data from Shared Preferences
		editor.clear();
		editor.commit();

		// After logout redirect user to Login Activity
		Intent i = new Intent(context, LoginActivity.class);

		// Closing all the Activities
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// Staring Login Activity
		context.startActivity(i);
	}

	// Check for login
	public boolean isUserLoggedIn() {
		return pref.getBoolean(IS_USER_LOGIN, false);
	}
}