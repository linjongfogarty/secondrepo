package com.example.mysqlphpjson;
 
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
 
public class ButtonActivity extends Activity {
 
  private RadioGroup radioGroup;
  private RadioButton radioNotificationButton;
  private Button btnChange;
 
  @Override
  public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.radio);
 
	addListenerOnButton();
 
  }
 
  public void addListenerOnButton() {
 
	radioGroup = (RadioGroup) findViewById(R.id.radioNotification);
	btnChange = (Button) findViewById(R.id.btnChange);
 
	btnChange.setOnClickListener(new OnClickListener() {
 
		@Override
		public void onClick(View v) {
 
		        // get selected radio button from radioGroup
			int selectedId = radioGroup.getCheckedRadioButtonId();
 
			// find the radiobutton by returned id
			radioNotificationButton = (RadioButton) findViewById(selectedId);
 
			Toast.makeText(ButtonActivity.this,
			radioNotificationButton.getText(), Toast.LENGTH_SHORT).show();
 
		}
 
	});
 
  }
}