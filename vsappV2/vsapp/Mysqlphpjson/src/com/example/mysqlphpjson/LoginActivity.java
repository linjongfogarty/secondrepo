package com.example.mysqlphpjson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
//import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	// Login Button
	Button btnLogin;

	// Fields where username and password will be inputed respectively
	EditText txtUsername, txtPassword;

	// User Session Manager Class
	UserSessionManager session;

	// Boolean for login validation
	Boolean isLoginValid = false;

	// JsonResult
	String jsonResult;

	// Map for linking Username and Password
	Map<String, String> userLoginMap = new HashMap<String, String>();

	// Maps
	Map<String, String> userFullNameMap = new HashMap<String, String>();
	Map<String, String> userLocationIDMap = new HashMap<String, String>();
	Map<String, String> userDepartmentMap = new HashMap<String, String>();
	Map<String, String> userImageMap = new HashMap<String, String>();

	// URL
	private String url = "http://10.66.47.47:8090/MySQLConnection.php";

	String username;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		accessWebService();

		// Connection conn = null;

		// User Session Manager
		session = new UserSessionManager(getApplicationContext());

		// get Email, Password input text
		txtUsername = (EditText) findViewById(R.id.txtUsername);
		txtPassword = (EditText) findViewById(R.id.txtPassword);

		Toast.makeText(getApplicationContext(),
				"User Login Status: " + session.isUserLoggedIn(),
				Toast.LENGTH_LONG).show();

		// User Login button
		btnLogin = (Button) findViewById(R.id.btnLogin);

		// Login button click event
		btnLogin.setOnClickListener(new View.OnClickListener() {

			// MainAcitvity object
			// MainActivity mainActivity = new MainActivity();

			@Override
			public void onClick(View arg0) {

				// Get username, password from EditText
				username = txtUsername.getText().toString();
				String password = txtPassword.getText().toString();

				// String matchedUserName = userLoginMap.
				String matchedPassword = userLoginMap.get(username);

				if (matchedPassword.equals(password)) {
					isLoginValid = true;
				}

				// Validate if username, password is filled
				if (username.trim().length() > 0
						&& password.trim().length() > 0) {

					if (isLoginValid) {

						String location_id = userLocationIDMap.get(username);
						String location = "";

						if (location_id.equals("1")) {
							location = "Cork";
						}

						if (location_id.equals("2")) {
							location = "London";
						}

						session.createUserLoginSession(username,
								userFullNameMap.get(username),
								userDepartmentMap.get(username), location,
								userImageMap.get(username));

						// Starting MainActivity
						Intent i = new Intent(getApplicationContext(),
								MainActivity.class);
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

						// Add new Flag to start new Activity
						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(i);

						finish();

					} else {

						// Username / password doesn't match
						Toast.makeText(getApplicationContext(),
								"Username/Password is incorrect",
								Toast.LENGTH_LONG).show();

					}
				} else {

					// User didn't entered username or password
					Toast.makeText(getApplicationContext(),
							"Please enter username and password",
							Toast.LENGTH_LONG).show();

				}

			}
		});
	}

	// Async Task to access the php web service
	private class JsonReadTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(params[0]);
			try {
				HttpResponse response = httpclient.execute(httppost);
				jsonResult = inputStreamToString(
						response.getEntity().getContent()).toString();
			}

			catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		private StringBuilder inputStreamToString(InputStream is) {
			String rLine = "";
			StringBuilder answer = new StringBuilder();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));

			try {
				while ((rLine = rd.readLine()) != null) {
					answer.append(rLine);
				}
			}

			catch (IOException e) {
				// e.printStackTrace();
				Toast.makeText(getApplicationContext(),
						"Error..." + e.toString(), Toast.LENGTH_LONG).show();
			}
			return answer;
		}

		@Override
		protected void onPostExecute(String result) {
			JsonRetrievalData();
		}
	}// end async task

	public void accessWebService() {
		JsonReadTask task = new JsonReadTask();
		// passes values for the urls string array
		task.execute(new String[] { url });
	}

	// store jsonSql attributes in maps
	@SuppressLint("InlinedApi")
	public void JsonRetrievalData() {
		try {
			JSONObject jsonResponse = new JSONObject(jsonResult);
			JSONArray jsonMainNode = jsonResponse.optJSONArray("users");

			for (int i = 0; i < jsonMainNode.length(); i++) {
				JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);

				userLoginMap.put(
						jsonChildNode.optString("username").toString(),
						jsonChildNode.optString("password").toString());
				userFullNameMap.put(jsonChildNode.optString("username")
						.toString(), jsonChildNode.optString("Full_Name")
						.toString());
				userLocationIDMap.put(jsonChildNode.optString("username")
						.toString(), jsonChildNode.optString("location_id")
						.toString());
				userDepartmentMap.put(jsonChildNode.optString("username")
						.toString(), jsonChildNode.optString("department")
						.toString());
				 userImageMap.put(jsonChildNode.optString("username"),
						 jsonChildNode.optString("image"));
			}
		} catch (JSONException e) {
			Toast.makeText(getApplicationContext(), "Error" + e.toString(),
					Toast.LENGTH_SHORT).show();
		}

	}
}
