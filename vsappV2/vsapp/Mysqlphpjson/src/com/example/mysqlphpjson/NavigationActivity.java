package com.example.mysqlphpjson;
 
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
 
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
public class NavigationActivity extends Activity {
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        /*ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*/
    }
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_actions, menu);
 
        return super.onCreateOptionsMenu(menu);
    }
    
    /**
     * On selecting action bar icons
     * */
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
        case R.id.timeline:
            // newsfeed
        	newsfeed ();
            return true;
        case R.id.action_search:
            // action search
            action_search();
            return true;
        case R.id.settings:
            // action settings
        	action_settings();
            return true;
        /*case R.id.action_help:
            // help action
            return true;*/
    
    
        case R.id.action_logout:
            // action logout
        	action_logout();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
 
    /**
     * Launching new activity
     * */
   /* private void LocationFound() {
        Intent i = new Intent(MainActivity.this, LocationFound.class);
        startActivity(i);
    }*/
    
    private void newsfeed(){
    	Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
    }
    
    private void action_search(){
    	Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
    }
    
    private void action_settings(){
    	Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
    }
    
    private void action_logout(){
    	Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
    }

}
