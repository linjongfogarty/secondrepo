package com.example.mysqlphpjson;
 
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
import java.util.ArrayList;
import java.util.HashMap;
 
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class SearchActivity extends Activity {
     
    // List view
    private ListView lv;
     
    // Listview Adapter
    ArrayAdapter<String> adapter;
     
    // Search EditText
    EditText inputSearch;
     
     
    // ArrayList for Listview
    ArrayList<HashMap<String, String>> userList;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);
         
        // Listview Data
        String users[] = {"", "", ""};
         
        lv = (ListView) findViewById(R.id.list_view);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
         
        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.user_profile, users);
        lv.setAdapter(adapter);       
         
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.main, menu);

        //Linny's added code for navigation bar
        inflater.inflate(R.menu.activity_main_actions, menu);

        return super.onCreateOptionsMenu(menu);

    }

    /**
     * On selecting action bar icons
     * This method accepts menu item as a parameter. Selected item
     * can be identified by using it's ID.
     * Handle action bar item clicks here. The action bar will
     * automatically handle clicks on the Home/Up button, so long
     * as you specify a parent activity in AndroidManifest.xml.
     */
    /**
     * On selecting action bar icons
     * */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.timeline:
                // newsfeed
                newsfeed ();
                return true;
            case R.id.action_search:
                // action search
                action_search();
                return true;
            case R.id.settings:
                // action settings
                action_settings();
                return true;
        /*case R.id.action_help:
            // help action
            return true;*/


            case R.id.action_logout:
                // action logout
                action_logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Launching new activity
     * */
   /* private void LocationFound() {
        Intent i = new Intent(MainActivity.this, LocationFound.class);
        startActivity(i);
    }*/

    private void newsfeed(){

        //Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    private void action_search(){

        //Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, SearchActivity.class);
        startActivity(i);
    }

    private void action_settings(){
        //Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, ButtonActivity.class);
        startActivity(i);
    }

    private void action_logout(){

        Toast.makeText(this, "This is user's profile/home page", Toast.LENGTH_SHORT).show();
    }
     
}