package com.example.mysqlphpjson;
 
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
 
public class ButtonActivity extends Activity {
 
  private RadioGroup radioGroup;
  private RadioButton radioNotificationButton;
  private Button btnChange;
 
  @Override
  public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.radio);
 
	addListenerOnButton();
 
  }


 
  public void addListenerOnButton() {
 
	radioGroup = (RadioGroup) findViewById(R.id.radioNotification);
	btnChange = (Button) findViewById(R.id.btnChange);
 
	btnChange.setOnClickListener(new OnClickListener() {
 
		@Override
		public void onClick(View v) {
 
		        // get selected radio button from radioGroup
			int selectedId = radioGroup.getCheckedRadioButtonId();
 
			// find the radiobutton by returned id
			radioNotificationButton = (RadioButton) findViewById(selectedId);
 
			Toast.makeText(ButtonActivity.this,
			radioNotificationButton.getText(), Toast.LENGTH_SHORT).show();
 
		}
 
	});
 
  }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        getMenuInflater().inflate(R.menu.main, menu);

        //Linny's added code for navigation bar
        inflater.inflate(R.menu.activity_main_actions, menu);

        return super.onCreateOptionsMenu(menu);

    }

    /**
     * On selecting action bar icons
     * This method accepts menu item as a parameter. Selected item
     * can be identified by using it's ID.
     * Handle action bar item clicks here. The action bar will
     * automatically handle clicks on the Home/Up button, so long
     * as you specify a parent activity in AndroidManifest.xml.
     */
    /**
     * On selecting action bar icons
     * */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.timeline:
                // newsfeed
                newsfeed ();
                return true;
            case R.id.action_search:
                // action search
                action_search();
                return true;
            case R.id.settings:
                // action settings
                action_settings();
                return true;
        /*case R.id.action_help:
            // help action
            return true;*/


            case R.id.action_logout:
                // action logout
                action_logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Launching new activity
     * */
   /* private void LocationFound() {
        Intent i = new Intent(MainActivity.this, LocationFound.class);
        startActivity(i);
    }*/

    private void newsfeed(){

        //Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    private void action_search(){

        //Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, SearchActivity.class);
        startActivity(i);
    }

    private void action_settings(){
        //Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, ButtonActivity.class);
        startActivity(i);
    }

    private void action_logout(){

        Toast.makeText(this,"This is user's profile/home page", Toast.LENGTH_SHORT).show();
    }
}