package com.vce.intern.navigationbar;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
 
/**
 * TODO: Document usage. Set proper Vision version in since tag.
 *
 * <p>
 * Copyright � 2014 VCE Company, LLC. All rights reserved.
 * </p>
 *
 * @since Vision x.y.z
 */
public class LocationFound extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location_found);
		
		//get action bar
		ActionBar actionBar = getActionBar();
		
		//Enabling Up/Back navigation
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		//actionBar.setIcon(R.drawable.ic_action_backspace);
	}
}
