package com.vce.intern.navigationbar;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
    	//getMenuInflater().inflate(R.menu.activity_main_actions, menu);
        //return true;
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.activity_main_actions, menu);
    	
        return super.onCreateOptionsMenu(menu);
    }


	/**
	 * On selecting action bar icons
	 * This method accepts menu item as a parameter. Selected item
   	 * can be identified by using it's ID.
     * Handle action bar item clicks here. The action bar will
     * automatically handle clicks on the Home/Up button, so long
     * as you specify a parent activity in AndroidManifest.xml.
	 */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);*/
    	
    	//Take appropriate action for each action item click
    	switch (item.getItemId()){
    		
    	case R.id.action_home:
    		//home
    		return true;
    	case R.id.settings_2_icon:
    		//settings
    		return true;
    	case R.id.action_email:
    		//location found
    		//LocationFound();
    		return true;
    	case R.id.action_person:
    		//refresh
    		return true;
    	case R.id.action_logout:
    		//log out
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	
    	}
    }
    
    /**
     * Launching new activity
     */
    /*private void LocationFound(){
    	Intent i = new Intent(MainActivity.this, LocationFound.class);
    	startActivity(i);
    }*/
}
